import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import {
  Landing,
  Question1,
  Question2,
  Question3,
  Question4,
  Question5,
  Results,
} from './pages'

const router = createBrowserRouter([
  {
    path: '/',
    element: <Landing />,
  },
  {
    path: '/question1',
    element: <Question1 />,
  },
  {
    path: '/question2',
    element: <Question2 />,
  },
  {
    path: '/question3',
    element: <Question3 />,
  },
  {
    path: '/question4',
    element: <Question4 />,
  },
  {
    path: '/question5',
    element: <Question5 />,
  },
  {
    path: '/results',
    element: <Results />,
  },
])

const App = () => {
  return <RouterProvider router={router} />
}
export default App
