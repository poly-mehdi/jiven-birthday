import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { IoGiftSharp } from 'react-icons/io5'
import { useState } from 'react'
import { air, eau, terre, feu, intro } from '../assets/question1'

const Question1 = () => {
  const [selectedAnswer, setSelectedAnswer] = useState('')

  const handleAnswerSelection = (answer) => {
    localStorage.setItem('question1', JSON.stringify(answer))
    setSelectedAnswer(answer)
  }
  return (
    <Wrapper>
      <header>
        <IoGiftSharp size={30} fill='white' />
        <p>Jiven's Birthday Quiz</p>
      </header>
      <div className='quiz-container'>
        <p className='quiz-question-number'>Question 1 of 5</p>
        <div className='progress-bar'>
          <div className='progress'></div>
        </div>
        <p className='sub-title'>Contexte</p>
        <img
          src={intro}
          alt='intro'
          className='img'
          style={{ marginBottom: '10px' }}
        />
        <div className='context-container'>
          <p className='context'>
            Dans un monde fantastique ravagé par la guerre depuis des siècles,
            où les royaumes se livrent des batailles sans fin pour le pouvoir et
            la domination, une prophétie ancienne émerge, annonçant qu'un seul
            individu pourra unifier les terres déchirées et instaurer enfin la
            paix tant désirée.
          </p>
          <p className='context'>
            Alors que les armées se préparent pour une nouvelle vague de
            conflits, une figure émerge des ombres : un jeune héros doté de
            pouvoirs mystérieux et d'une détermination sans faille. Convaincu
            que la conquête du monde est la seule voie vers la paix, il lève une
            armée composée de toutes les races et factions, des humains aux
            elfes, des nains aux créatures magiques.
          </p>
          <p className='context'>
            Dans cette lutte épique pour la paix, le héros devra faire des choix
            difficiles et sacrifier beaucoup.
          </p>
        </div>

        <h2 className='quiz-question'>Choisis un héros :</h2>
        <div className='btn-container'>
          <img src={air} alt='air' />
          <div
            className={`btn ${selectedAnswer === 'A' && 'selected'}`}
            onClick={() => handleAnswerSelection('A')}
          >
            <span>A</span>Maitre de l'air
          </div>
          <img src={terre} alt='terre' />
          <div
            className={`btn ${selectedAnswer === 'D' && 'selected'}`}
            onClick={() => handleAnswerSelection('D')}
          >
            <span>B</span>Maitre de la terre
          </div>
          <img src={eau} alt='eau' />
          <div
            className={`btn ${selectedAnswer === 'C' && 'selected'}`}
            onClick={() => handleAnswerSelection('C')}
          >
            <span>C</span>Maitre de l'eau
          </div>
          <img src={feu} alt='feu' />
          <div
            className={`btn ${selectedAnswer === 'B' && 'selected'}`}
            onClick={() => handleAnswerSelection('B')}
          >
            <span>D</span>Maitre du feu
          </div>
        </div>
      </div>
      <Link to='/question2' className='btn-submit'>
        Soumettre
      </Link>
    </Wrapper>
  )
}

const Wrapper = styled.section`
  width: 90vw;
  margin: 0 auto;
  display: flex;
  flex-direction: column;

  /* * {
    border: 1px solid red;
  } */

  header {
    display: flex;
    align-items: center;
    gap: 1rem;
    padding-bottom: 1rem;
    border-bottom: 1px solid #313e51;

    p {
      font-size: 1.2rem;
      font-weight: 400;
      color: white;
    }
  }

  .quiz-container {
    padding: 1rem 0;

    .progress-bar {
      background-color: #3b4e66;
      padding: 5px 5px;
      border-radius: 10px;
      margin: 1rem 0;

      .progress {
        height: 7px;
        background-color: #a729f5;
        border-radius: 5px;
        width: 20%;
      }
    }

    .quiz-question-number {
      color: #abc1e1;
      font-size: 1rem;
      font-style: italic;
      font-weight: 400;
    }

    .sub-title {
      font-size: 1.2rem;
      font-weight: 500;
      color: white;
      margin: 1rem 0;
    }

    .context-container {
      .context {
        text-align: justify;
        color: white;
      }
    }

    .quiz-question {
      font-size: 1.5rem;
      font-weight: 500;
      color: white;
      margin: 1rem 0;
    }

    .btn-container {
      display: flex;
      flex-direction: column;
      gap: 1rem;

      .btn {
        padding: 0.75rem 1rem;
        border-radius: 5px;
        background-color: #3b4e66;
        color: white;
        font-size: 1.2rem;
        font-weight: bold;
        cursor: pointer;
        transition: all 0.3s ease;

        display: flex;
        gap: 1rem;
        align-items: center;

        span {
          background-color: #f4f6fa;
          color: #616c7f;
          padding: 0.5rem 0.75rem;
          border-radius: 5px;
        }
      }

      .btn.selected {
        /* Style lorsque le bouton est sélectionné */
        border: 2px solid #a729f5; /* Bordure violette */
      }
    }
  }

  .btn-submit {
    padding: 1rem 1.5rem;
    border-radius: 10px;
    background-color: #a729f5;
    color: white;
    font-size: 1.2rem;
    font-weight: bold;
    cursor: pointer;
    transition: all 0.3s ease;
    text-align: center;
    margin-top: 1rem;
  }
`

export default Question1
