import styled from 'styled-components'
import { useState, useEffect } from 'react'
import ScrollToTop from '../components/ScrollToTop'
import Fireworks from '@fireworks-js/react'

import { saut, cirque, barman, ps5, naruto } from '../assets/results'

const Results = () => {
  const [results, setResults] = useState({})
  const [mostCommonAnswer, setMostCommonAnswer] = useState('')

  useEffect(() => {
    const q1Results = JSON.parse(localStorage.getItem('question1'))
    const q2Results = JSON.parse(localStorage.getItem('question2'))
    const q3Results = JSON.parse(localStorage.getItem('question3'))
    const q4Results = JSON.parse(localStorage.getItem('question4'))
    const q5Results = JSON.parse(localStorage.getItem('question5'))

    // Combiner toutes les réponses dans un seul tableau
    const allResults = [q1Results, q2Results, q3Results, q4Results, q5Results]

    // Initialiser un objet pour compter les occurrences de chaque réponse
    const answerCounts = {
      A: 0,
      B: 0,
      C: 0,
      D: 0,
    }

    // Fonction pour mettre à jour les compteurs d'occurrences pour une réponse donnée
    const updateAnswerCounts = (results) => {
      if (results) {
        answerCounts[results]++
      }
    }

    // Mettre à jour les compteurs pour chaque résultat
    allResults.forEach(updateAnswerCounts)

    // Trouver la réponse avec le plus grand nombre d'occurrences
    let mostCommonAnswer = ''
    let maxCount = 0

    for (const answer in answerCounts) {
      if (answerCounts[answer] > maxCount) {
        maxCount = answerCounts[answer]
        mostCommonAnswer = answer
      }
    }

    setMostCommonAnswer(mostCommonAnswer)
  }, [])
  return (
    <Wrapper>
      <Fireworks
        width={800}
        height={600}
        colors={['#A729F5', '#3B4D66', '#EE5454']}
        style={{ position: 'absolute', top: 0, left: 0, zIndex: 100 }}
      />
      <ScrollToTop />
      <h1>Résultats</h1>
      <img src={naruto} alt='naruto' className='img result-img' />
      <div className='result-text'>
        <h3>Félicitations</h3>
        {mostCommonAnswer === 'A' && (
          <>
            <p>Tu as gagné un saut à l'élastique</p>
            <img src={saut} alt='saut' className='img' />
          </>
        )}
        {mostCommonAnswer === 'B' && (
          <>
            <p>Tu as gagné 2 places pour le cirque du soleil KURIOS</p>
            <img src={cirque} alt='cirque' className='img' />
          </>
        )}
        {mostCommonAnswer === 'C' && (
          <>
            <p>Tu as gagné une formation de Barman</p>
            <img src={barman} alt='barman' className='img' />
          </>
        )}
        {mostCommonAnswer === 'D' && (
          <>
            <p>Tu as gagné une PS5</p>
            <img src={ps5} alt='ps5' className='img' />
          </>
        )}
      </div>
    </Wrapper>
  )
}

const Wrapper = styled.section`
  width: 90vw;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 2rem;

  h1 {
    font-size: 2rem;
    font-weight: 400;
    color: white;
  }

  .result-img {
    width: 22rem;
  }

  .result-text {
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 0.5rem;
    font-family: Georgia, 'Times New Roman', Times, serif;

    h3 {
      font-size: 1.5rem;
      font-weight: 400;
      color: white;
    }

    p {
      font-size: 1.2rem;
      font-weight: 400;
      color: white;
      text-align: center;
    }
  }
`
export default Results
