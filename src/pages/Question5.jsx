import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { IoGiftSharp } from 'react-icons/io5'
import { useState } from 'react'
import ScrollToTop from '../components/ScrollToTop'
import {
  redemption,
  prison,
  exile,
  epargne,
  landscape,
} from '../assets/question5'

const Question5 = () => {
  const [selectedAnswer, setSelectedAnswer] = useState('')

  const handleAnswerSelection = (answer) => {
    localStorage.setItem('question5', JSON.stringify(answer))
    setSelectedAnswer(answer)
  }
  return (
    <Wrapper>
      <ScrollToTop />
      <header>
        <IoGiftSharp size={30} fill='white' />
        <p>Jiven's Birthday Quiz</p>
      </header>
      <div className='quiz-container'>
        <p className='quiz-question-number'>Question 5 of 5</p>
        <div className='progress-bar'>
          <div className='progress'></div>
        </div>
        <img
          src={landscape}
          alt='landscape'
          className='img'
          style={{ marginBottom: '10px' }}
        />
        <h2 className='quiz-question'>
          Après avoir triomphé dans un combat acharné contre ton adversaire,
          quelle sera ta décision concernant les survivants ?
        </h2>
        <div className='btn-container'>
          <img src={redemption} alt='redemption' />
          <div
            className={`btn ${selectedAnswer === 'C' && 'selected'}`}
            onClick={() => handleAnswerSelection('C')}
          >
            <span>A</span>Les intègres-tu à ton propre peuple, offrant une
            chance de rédemption et d'unité.
          </div>
          <img src={prison} alt='prison' />
          <div
            className={`btn ${selectedAnswer === 'A' && 'selected'}`}
            onClick={() => handleAnswerSelection('A')}
          >
            <span>B</span>Les emprisonnes-tu, assurant ainsi qu'ils ne pourront
            plus semer le chaos.
          </div>
          <img src={exile} alt='exile' />
          <div
            className={`btn ${selectedAnswer === 'B' && 'selected'}`}
            onClick={() => handleAnswerSelection('B')}
          >
            <span>C</span>Les exiles-tu vers des terres lointaines, leur donnant
            une chance de recommencer loin du conflit.
          </div>
          <img src={epargne} alt='epargne' />
          <div
            className={`btn ${selectedAnswer === 'D' && 'selected'}`}
            onClick={() => handleAnswerSelection('D')}
          >
            <span>D</span>Les épargnes-tu, reconnaissant que même parmi les
            ennemis, il y a ceux qui méritent une seconde chance.
          </div>
        </div>
      </div>
      <Link to='/results' className='btn-submit'>
        Soumettre
      </Link>
    </Wrapper>
  )
}

const Wrapper = styled.section`
  width: 90vw;
  margin: 0 auto;
  display: flex;
  flex-direction: column;

  /* * {
    border: 1px solid red;
  } */

  header {
    display: flex;
    align-items: center;
    gap: 1rem;
    padding-bottom: 1rem;
    border-bottom: 1px solid #313e51;

    p {
      font-size: 1.2rem;
      font-weight: 400;
      color: white;
    }
  }

  .quiz-container {
    padding: 1rem 0;

    .progress-bar {
      background-color: #3b4e66;
      padding: 5px 5px;
      border-radius: 10px;
      margin: 1rem 0;

      .progress {
        height: 7px;
        background-color: #a729f5;
        border-radius: 5px;
        width: 100%;
      }
    }
    .quiz-question-number {
      color: #abc1e1;
      font-size: 1rem;
      font-style: italic;
      font-weight: 400;
    }

    .sub-title {
      font-size: 1.2rem;
      font-weight: 500;
      color: white;
      margin: 1rem 0;
    }

    .context-container {
      margin-bottom: 1rem;
      .context {
        text-align: justify;
        color: white;
      }
    }

    .quiz-question {
      font-size: 1.5rem;
      font-weight: 500;
      color: white;
      margin: 1rem 0;
    }

    .btn-container {
      display: flex;
      flex-direction: column;
      gap: 1rem;

      .btn {
        padding: 0.75rem 1rem;
        border-radius: 5px;
        background-color: #3b4e66;
        color: white;
        font-size: 1.2rem;
        font-weight: bold;
        cursor: pointer;
        transition: all 0.3s ease;

        display: flex;
        gap: 1rem;
        align-items: center;

        span {
          background-color: #f4f6fa;
          color: #616c7f;
          padding: 0.5rem 0.75rem;
          border-radius: 5px;
        }
      }
      .btn.selected {
        /* Style lorsque le bouton est sélectionné */
        border: 2px solid #a729f5; /* Bordure violette */
      }
    }
  }

  .btn-submit {
    padding: 1rem 1.5rem;
    border-radius: 10px;
    background-color: #a729f5;
    color: white;
    font-size: 1.2rem;
    font-weight: bold;
    cursor: pointer;
    transition: all 0.3s ease;
    text-align: center;
    margin-top: 1rem;
  }
`

export default Question5
