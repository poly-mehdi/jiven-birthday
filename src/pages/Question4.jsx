import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { IoGiftSharp } from 'react-icons/io5'
import { useState } from 'react'
import ScrollToTop from '../components/ScrollToTop'
import { hero, force, intelligence, agilite, art } from '../assets/question4'

const Question4 = () => {
  const [selectedAnswer, setSelectedAnswer] = useState('')

  const handleAnswerSelection = (answer) => {
    localStorage.setItem('question4', JSON.stringify(answer))
    setSelectedAnswer(answer)
  }
  return (
    <Wrapper>
      <ScrollToTop />
      <header>
        <IoGiftSharp size={30} fill='white' />
        <p>Jiven's Birthday Quiz</p>
      </header>
      <div className='quiz-container'>
        <p className='quiz-question-number'>Question 4 of 5</p>
        <div className='progress-bar'>
          <div className='progress'></div>
        </div>
        <p className='sub-title'>Contexte</p>
        <img
          src={hero}
          alt='hero'
          className='img'
          style={{ marginBottom: '10px' }}
        />
        <div className='context-container'>
          <p className='context'>
            Dans le duel crucial pour le contrôle du pouvoir, la dernière
            faction adverse te défie en duel contre son meilleur combattant.
          </p>
        </div>
        <h2 className='quiz-question'>
          Parmi ces aptitudes en combat, laquelle choisis-tu pour t'assurer la
          victoire :
        </h2>
        <div className='btn-container'>
          <img src={agilite} alt='agilite' />
          <div
            className={`btn ${selectedAnswer === 'A' && 'selected'}`}
            onClick={() => handleAnswerSelection('A')}
          >
            <span>A</span>Une agilité foudroyante, esquivant habilement les
            attaques ennemies.
          </div>
          <img src={force} alt='force' />
          <div
            className={`btn ${selectedAnswer === 'D' && 'selected'}`}
            onClick={() => handleAnswerSelection('D')}
          >
            <span>B</span>Une force inébranlable, capable de briser la défense
            de ton adversaire avec des coups puissants.
          </div>
          <img src={intelligence} alt='intelligence' />
          <div
            className={`btn ${selectedAnswer === 'C' && 'selected'}`}
            onClick={() => handleAnswerSelection('C')}
          >
            <span>C</span>Une intelligence tactique, utilisant la ruse et la
            stratégie pour prendre l'avantage sur ton adversaire.
          </div>
          <img src={art} alt='art' />
          <div
            className={`btn ${selectedAnswer === 'B' && 'selected'}`}
            onClick={() => handleAnswerSelection('B')}
          >
            <span>D</span>Une maîtrise des arts martiaux, combinant habilement
            attaque et défense dans un ballet mortel.
          </div>
        </div>
      </div>
      <Link to='/question5' className='btn-submit'>
        Soumettre
      </Link>
    </Wrapper>
  )
}

const Wrapper = styled.section`
  width: 90vw;
  margin: 0 auto;
  display: flex;
  flex-direction: column;

  /* * {
    border: 1px solid red;
  } */

  header {
    display: flex;
    align-items: center;
    gap: 1rem;
    padding-bottom: 1rem;
    border-bottom: 1px solid #313e51;

    p {
      font-size: 1.2rem;
      font-weight: 400;
      color: white;
    }
  }

  .quiz-container {
    padding: 1rem 0;

    .progress-bar {
      background-color: #3b4e66;
      padding: 5px 5px;
      border-radius: 10px;
      margin: 1rem 0;

      .progress {
        height: 7px;
        background-color: #a729f5;
        border-radius: 5px;
        width: 80%;
      }
    }

    .quiz-question-number {
      color: #abc1e1;
      font-size: 1rem;
      font-style: italic;
      font-weight: 400;
    }

    .sub-title {
      font-size: 1.2rem;
      font-weight: 500;
      color: white;
      margin: 1rem 0;
    }

    .context-container {
      .context {
        text-align: justify;
        color: white;
      }
    }

    .quiz-question {
      font-size: 1.5rem;
      font-weight: 500;
      color: white;
      margin: 1rem 0;
    }

    .btn-container {
      display: flex;
      flex-direction: column;
      gap: 1rem;

      .btn {
        padding: 0.75rem 1rem;
        border-radius: 5px;
        background-color: #3b4e66;
        color: white;
        font-size: 1.2rem;
        font-weight: bold;
        cursor: pointer;
        transition: all 0.3s ease;

        display: flex;
        gap: 1rem;
        align-items: center;

        span {
          background-color: #f4f6fa;
          color: #616c7f;
          padding: 0.5rem 0.75rem;
          border-radius: 5px;
        }
      }
      .btn.selected {
        /* Style lorsque le bouton est sélectionné */
        border: 2px solid #a729f5; /* Bordure violette */
      }
    }
  }

  .btn-submit {
    padding: 1rem 1.5rem;
    border-radius: 10px;
    background-color: #a729f5;
    color: white;
    font-size: 1.2rem;
    font-weight: bold;
    cursor: pointer;
    transition: all 0.3s ease;
    text-align: center;
    margin-top: 1rem;
  }
`

export default Question4
