import { Link } from 'react-router-dom'
import styled from 'styled-components'
import Jiven from '../assets/jiven-head.png'

const Landing = () => {
  return (
    <Wrapper>
      <div className='landing-container'>
        <div className='landing-img'>
          <img src={Jiven} alt='jiven' className='img' />
        </div>
        <div className='landing-text-container'>
          <div className='landing-text'>
            <div className='title'>
              <p className='title-p'>Quizz d'anniversaire</p>
              <p className='description'>
                Il est temps de determiner ton cadeau.
              </p>
            </div>
          </div>
          <Link to='/question1' className='btn-start'>
            Commencer le quizz
          </Link>
        </div>
      </div>
    </Wrapper>
  )
}

const Wrapper = styled.section`
  width: 90vw;
  margin: 0 auto;
  height: 100vh;
  display: grid;
  place-items: center;

  /* * {
    border: 1px solid red;
  } */

  .landing-container {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    gap: 2rem;
    text-align: center;

    .landing-img {
      width: 12rem;
    }

    .landing-text-container {
      display: flex;
      flex-direction: column;
      align-items: center;
      gap: 2rem;

      .landing-text {
        .title {
          display: flex;
          flex-direction: column;
          align-items: center;
          gap: 0.5rem;

          .title-p {
            color: white;
            font-size: 2rem;
            font-weight: bold;
          }

          .description {
            color: white;
          }
        }
      }

      .btn-start {
        padding: 1rem 1.5rem;
        border-radius: 5px;
        background-color: #a729f5;
        color: white;
        font-size: 1.2rem;
        font-weight: bold;
        cursor: pointer;
        transition: all 0.3s ease;
      }
    }
  }
`

export default Landing
